import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faEnvelope,
    faPhone,
    faSearch,
    faStar,
    faBars,
    faMapMarkerAlt,
    faShoppingCart
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
    faFacebookF,
    faYoutube,
    faGooglePlusG,
    faTwitter
} from '@fortawesome/free-brands-svg-icons';

library.add(
    faEnvelope,
    faPhone,
    faFacebookF,
    faTwitter,
    faGooglePlusG,
    faYoutube,
    faSearch,
    faStar,
    faBars,
    faMapMarkerAlt,
    faShoppingCart
);
import VueMasonry from 'vue-masonry-css';

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.use(BootstrapVue);
Vue.use(VueMasonry);

Vue.config.productionTip = false;

new Vue({
    store,
    router,
    render: h => h(App)
}).$mount('#app');
