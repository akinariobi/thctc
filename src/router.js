import Vue from 'vue';
import Router from 'vue-router';
import Home from './components/Home.vue';
import Courses from './components/Courses.vue';
import Contact from './components/Contact.vue';
import Resources from './components/Resources.vue';
import CareHomes from './components/CareHomesCourses.vue';
import HomeCare from './components/HomeCareCourses.vue';
import CourseDescription from './components/CourseDescription.vue';
import Cart from './components/Cart.vue';
import CoursesSearch from './components/CoursesSearch.vue';
import FreeStuff from './components/FreeStuff.vue';
import Shop from './components/Shop.vue';
import ShopItemDescription from './components/ShopItemDescription.vue';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/courses',
            name: 'courses',
            component: Courses,
            children: [
                {
                    path: 'home-care',
                    component: HomeCare
                },
                {
                    path: 'care-homes',
                    component: CareHomes
                },
                {
                    path: ':name',
                    component: CourseDescription
                }
            ]
        },
        {
            path: '/about/:name',
            name: 'test',
            component: CourseDescription
        },
        {
            path: '/contact',
            name: 'contact',
            component: Contact
        },
        {
            path: '/resources',
            name: 'resources',
            component: Resources
        },
        {
            path: '/cart',
            name: 'cart',
            component: Cart
        },
        {
            path: '/search',
            name: 'search',
            component: CoursesSearch
        },
        {
            path: '/free',
            name: 'free',
            component: FreeStuff
        },
        {
            path: '/shop',
            name: 'shop',
            component: Shop,
            children: [
                {
                    path: ':name',
                    component: ShopItemDescription
                }
            ]
        }
    ]
});
