import Vuex from 'vuex';
import Vue from 'vue';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        cartItems: [],
        totalQuantity: 0,
        searchItems: []
    }
});

export default store;
