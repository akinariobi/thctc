const freeStuff = [
    {
        title: 'True Happiness Is ',
        href: 'https://www.thctc.co.uk/download/true-happiness-is/?wpdmdl=310'
    },
    {
        title: 'The guide to filling care home beds ',
        href:
            'https://www.thctc.co.uk/download/the-guide-to-filling-care-home-beds/?wpdmdl=308'
    },
    {
        title: 'Successful steps to person centred activity ',
        href:
            'https://www.thctc.co.uk/download/successful-steps-to-person-centred-activity/?wpdmdl=306'
    },
    {
        title: 'Successful person centred care and built environments ',
        href:
            'https://www.thctc.co.uk/download/successful-person-centred-care-and-built-environments/?wpdmdl=304'
    },
    {
        title: 'Successful activity provision for care homes ',
        href:
            'https://www.thctc.co.uk/download/successful-activity-provision-for-care-homes/?wpdmdl=302'
    },
    {
        title: 'Successful activites for different stages of dementia ',
        href:
            'https://www.thctc.co.uk/download/successful-activites-for-different-stages-of-dementia/?wpdmdl=300'
    },
    {
        title: 'Report on seated exercise ',
        href:
            'https://www.thctc.co.uk/download/report-on-seated-exercise/?wpdmdl=298'
    },
    {
        title: 'Nutrition ',
        href: 'https://www.thctc.co.uk/download/nutrition/?wpdmdl=295'
    },
    {
        title: 'Injury ',
        href: 'https://www.thctc.co.uk/download/injury/?wpdmdl=293'
    },
    {
        title: 'Dementia ',
        href: 'https://www.thctc.co.uk/download/dementia/?wpdmdl=486'
    },
    {
        title: 'TILEE Assessment ',
        href: 'https://www.thctc.co.uk/download/tilee-assessment/?wpdmdl=548'
    },
    {
        title: 'Therapeutic Activities and Dementia ',
        href:
            'https://www.thctc.co.uk/download/therapeutic-activities-dementia/?wpdmdl=546'
    },
    {
        title: 'Supervision and Coaching ',
        href:
            'https://www.thctc.co.uk/download/supervision-and-coaching-2/?wpdmdl=544'
    },
    {
        title: 'Supervision and Coaching ',
        href:
            'https://www.thctc.co.uk/download/supervision-and-coaching/?wpdmdl=542'
    },
    {
        title: 'Stroke ',
        href: 'https://www.thctc.co.uk/download/stroke/?wpdmdl=540'
    },
    {
        title: 'SMART Goals ',
        href: 'https://www.thctc.co.uk/download/smart-goals/?wpdmdl=538'
    },
    {
        title: 'Responding to Challenging Behaviour ',
        href:
            'https://www.thctc.co.uk/download/responding-challenging-behaviour/?wpdmdl=536'
    },
    {
        title: 'Reminiscence and Life History ',
        href:
            'https://www.thctc.co.uk/download/reminiscence-life-history/?wpdmdl=534'
    },
    {
        title: 'Person Centred Care ',
        href: 'https://www.thctc.co.uk/download/person-centred-care/?wpdmdl=532'
    },
    {
        title: "Parkinson's Experience ",
        href:
            'https://www.thctc.co.uk/download/parkinsons-experience/?wpdmdl=530'
    },
    {
        title: 'Memory Training ',
        href: 'https://www.thctc.co.uk/download/memory-training/?wpdmdl=528'
    },
    {
        title: 'Managing Activity Provision Workbook ',
        href:
            'https://www.thctc.co.uk/download/managing-activity-provision-workbook/?wpdmdl=526'
    },
    {
        title: 'Lone Working ',
        href: 'https://www.thctc.co.uk/download/lone-working/?wpdmdl=524'
    },
    {
        title: 'Leadership Traits ',
        href: 'https://www.thctc.co.uk/download/leadership-traits/?wpdmdl=522'
    },
    {
        title: 'How to guide people with sight problems ',
        href:
            'https://www.thctc.co.uk/download/guide-people-sight-problems/?wpdmdl=520'
    },
    {
        title: 'Handling Threatening Situations ',
        href:
            'https://www.thctc.co.uk/download/handling-threatening-situations/?wpdmdl=518'
    },
    {
        title: 'General Par Q Handout ',
        href:
            'https://www.thctc.co.uk/download/general-par-q-handout/?wpdmdl=516'
    },
    {
        title: 'Functional Assessment Measure ',
        href:
            'https://www.thctc.co.uk/download/functional-assessment-measure/?wpdmdl=514'
    },
    {
        title: 'Filling Beds Workbook ',
        href:
            'https://www.thctc.co.uk/download/filling-beds-workbook/?wpdmdl=512'
    },
    {
        title: 'Falls Prevention Exercise Leader Training ',
        href:
            'https://www.thctc.co.uk/download/falls-prevention-exercise-leader-training/?wpdmdl=510'
    },
    {
        title: 'Experimental Learning ',
        href:
            'https://www.thctc.co.uk/download/experimental-learning/?wpdmdl=508'
    },
    {
        title: 'Exercise and Mobility ',
        href:
            'https://www.thctc.co.uk/download/exercise-and-mobility/?wpdmdl=506'
    },
    {
        title: 'Disc Pressures ',
        href: 'https://www.thctc.co.uk/download/disc-pressures/?wpdmdl=504'
    },
    {
        title: 'Dignity in Care ',
        href: 'https://www.thctc.co.uk/download/dignity-in-care/?wpdmdl=502'
    },
    {
        title: 'Diabetes Awareness ',
        href: 'https://www.thctc.co.uk/download/diabetes-awareness/?wpdmdl=496'
    },
    {
        title: 'Stages of Dementia ',
        href: 'https://www.thctc.co.uk/download/stages-of-dementia/?wpdmdl=494'
    },
    {
        title: 'Depression in Later Life ',
        href:
            'https://www.thctc.co.uk/download/depression-later-life/?wpdmdl=492'
    },
    {
        title: 'Dementia ',
        href: 'https://www.thctc.co.uk/download/dementia/?wpdmdl=486'
    },
    {
        title: 'Creative Activities ',
        href: 'https://www.thctc.co.uk/download/creative-activities/?wpdmdl=484'
    },
    {
        title: 'Fall Risk Assessment Tool ',
        href:
            'https://www.thctc.co.uk/download/fall-risk-assessment-tool/?wpdmdl=482'
    },
    {
        title: 'Bariatric Moving and Handling ',
        href:
            'https://www.thctc.co.uk/download/bariatric-moving-handling/?wpdmdl=480'
    },
    {
        title: 'ACTIVITY ASSESSMENT FOR DEMENTIA ',
        href:
            'https://www.thctc.co.uk/download/activity-assessment-dementia/?wpdmdl=478'
    },
    {
        title: 'The Rules of running a successful care home ',
        href:
            'https://www.thctc.co.uk/download/rules-running-successful-care-home/?wpdmdl=593'
    },
    {
        title: 'Time Management ',
        href: 'https://www.thctc.co.uk/download/time-management/?wpdmdl=658'
    },
    {
        title: 'Reablement ',
        href: 'https://www.thctc.co.uk/download/reablement/?wpdmdl=666'
    },
    {
        title: 'Affiliate advertising for care ',
        href:
            'https://www.thctc.co.uk/download/affiliate-advertising-care/?wpdmdl=669'
    },
    {
        title: 'Having Difficult Conversations ',
        href:
            'https://www.thctc.co.uk/download/having-difficult-conversations/?wpdmdl=688'
    },
    {
        title: 'Moving and Handling Challenging Behaviour Part 1 ',
        href:
            'https://www.thctc.co.uk/download/moving-handling-challenging-behaviour-part-1/?wpdmdl=684'
    },
    {
        title: 'Moving and Handling Challenging Behaviour Part 2 ',
        href:
            'https://www.thctc.co.uk/download/moving-handling-challenging-behaviour-part-2/?wpdmdl=686'
    },
    {
        title: 'Coaching, Mentoring and Supervision ',
        href:
            'https://www.thctc.co.uk/download/coaching-mentoring-supervision/?wpdmdl=701'
    },
    {
        title: 'Equality and Diversity ',
        href:
            'https://www.thctc.co.uk/download/equality-and-diversity/?wpdmdl=704'
    },
    {
        title: 'The Value of Sleep ',
        href: 'https://www.thctc.co.uk/download/the-value-of-sleep/?wpdmdl=709'
    }
];

export default freeStuff;
