const careHomesLinks = [
    {
        title: 'Reablement Awareness',
        href: 'https://www.thctc.co.uk/product/home-care/reablement-awareness/'
    },
    {
        title: 'Activities for those in the late stages of Dementia ',
        href:
            'https://www.thctc.co.uk/product/care-home/activities-for-those-in-the-late-stages-of-dementia/'
    },
    {
        title: 'Massage Therapy for Older People',
        href:
            'https://www.thctc.co.uk/product/care-home/massage-therapy-for-older-people/'
    },
    {
        title: 'Moving & handling people with behaviours that challenge',
        href:
            'https://www.thctc.co.uk/product/home-care/moving-handling-people-with-behaviours-that-challenge/'
    },
    {
        title: 'Subcutaneous syringe pumps in palliative care ',
        href:
            'https://www.thctc.co.uk/product/care-home/subcutaneous-syringe-pumps-in-palliative-care/'
    },
    {
        title: 'Nurse Verification of Expected Death ',
        href:
            'https://www.thctc.co.uk/product/care-home/nurse-verification-of-expected-death/'
    },
    {
        title: 'Social Care Train the Trainer',
        href:
            'https://www.thctc.co.uk/product/home-care/social-care-train-the-trainer/'
    },
    {
        title: 'Reablement Train the Trainer',
        href:
            'https://www.thctc.co.uk/product/home-care/reablement-train-the-trainer/'
    },
    {
        title: 'Getting staff to attend training ',
        href:
            'https://www.thctc.co.uk/product/home-care/getting-staff-to-attend-training/'
    },
    {
        title: 'Fire Marshal Training',
        href: 'https://www.thctc.co.uk/product/care-home/fire-marshal-training/'
    },
    {
        title: 'HIV Awareness',
        href: 'https://www.thctc.co.uk/product/home-care/hiv-awareness/'
    },
    {
        title: 'Chair Yoga',
        href: 'https://www.thctc.co.uk/product/care-home/chair-yoga/'
    },
    {
        title: 'Introduction to Autism',
        href:
            'https://www.thctc.co.uk/product/home-care/introduction-to-autism/'
    },
    {
        title: 'Communication and Dementia ',
        href:
            'https://www.thctc.co.uk/product/home-care/communication-and-dementia/'
    },
    {
        title: 'Introduction to Autism ',
        href:
            'https://www.thctc.co.uk/product/care-home/introduction-to-autism-2/'
    },
    {
        title: 'Meaningful Activities and Dementia ',
        href:
            'https://www.thctc.co.uk/product/meaningful-activities-and-dementia-2/'
    },
    {
        title: 'Memory Training',
        href: 'https://www.thctc.co.uk/product/home-care/memory-training/'
    },
    {
        title: 'Communication and Autism',
        href:
            'https://www.thctc.co.uk/product/home-care/communication-and-autism-2/'
    },
    {
        title: 'Lone Working ',
        href: 'https://www.thctc.co.uk/product/home-care/lone-working/'
    },
    {
        title: 'Train the trainer prevention and control of infection',
        href:
            'https://www.thctc.co.uk/product/home-care/train-the-trainer-prevention-and-control-of-infection/'
    },
    {
        title: 'Managing difficult staff',
        href:
            'https://www.thctc.co.uk/product/home-care/managing-difficult-staff/'
    },
    {
        title: 'Train the Trainer',
        href: 'https://www.thctc.co.uk/product/home-care/train-the-trainer/'
    },
    {
        title: 'Person Centred Care ',
        href: 'https://www.thctc.co.uk/product/home-care/person-centred-care/'
    },
    {
        title: "Parkinson's Awareness",
        href: 'https://www.thctc.co.uk/product/home-care/parkinsons-awareness/'
    },
    {
        title: 'Communication and Brain Injury',
        href:
            'https://www.thctc.co.uk/product/care-home/communication-and-brain-injury/'
    },
    {
        title: 'The Elderly Falls Experience',
        href:
            'https://www.thctc.co.uk/product/care-home/the-elderly-falls-experience-2/'
    },
    {
        title: 'Motivational interviewing and alcohol abuse',
        href:
            'https://www.thctc.co.uk/product/care-home/motivational-interviewing-and-alcohol-abuse/'
    },
    {
        title: 'Safeguarding of Vulnerable Adults     ',
        href:
            'https://www.thctc.co.uk/product/home-care/safeguarding-of-vulnerable-adults/'
    },
    {
        title: ' Risk assessment for community based care workers ',
        href:
            'https://www.thctc.co.uk/product/home-care/risk-assessment-for-community-based-care-workers/'
    },
    {
        title: 'Promoting physical activity & mobility in older people   ',
        href:
            'https://www.thctc.co.uk/product/care-home/promoting-physical-activity-mobility-in-older-people/'
    },
    {
        title: 'Manual Handling of objects',
        href:
            'https://www.thctc.co.uk/product/home-care/manual-handling-of-objects/'
    },
    {
        title: 'Falls Prevention Advanced Practice ',
        href:
            'https://www.thctc.co.uk/product/care-home/falls-prevention-advanced-practice/'
    },
    {
        title: 'Train the trainer - safeguarding adults  ',
        href:
            'https://www.thctc.co.uk/product/care-home/train-the-trainer-safeguarding-adults/'
    },
    {
        title: ' Dignity in Care Champions Training',
        href:
            'https://www.thctc.co.uk/product/home-care/dignity-in-care-champions-training/'
    },
    {
        title: 'Falls Prevention Advanced Practice Train the trainer  ',
        href:
            'https://www.thctc.co.uk/product/care-home/falls-prevention-advanced-practice-train-the-trainer/'
    },
    {
        title: 'Professional Boundaries in Care',
        href:
            'https://www.thctc.co.uk/product/home-care/professional-boundaries-in-care-2/'
    },
    {
        title:
            'Filling Beds - Practical marketing and sales for care home managers',
        href:
            'https://www.thctc.co.uk/product/care-home/filling-beds-practical-marketing-and-sales-for-care-home-managers/'
    },
    {
        title: 'Moving and Handling Train the Trainer    ',
        href:
            'https://www.thctc.co.uk/product/home-care/moving-and-handling-train-the-trainer/'
    },
    {
        title: 'Handling Threatening Situations ',
        href:
            'https://www.thctc.co.uk/product/home-care/handling-threatening-situations/'
    },
    {
        title: 'Introduction to Learning Disability',
        href:
            'https://www.thctc.co.uk/product/home-care/introduction-to-learning-disability/'
    },
    {
        title: 'Parkinson’s Disease: Advanced Practice',
        href:
            'https://www.thctc.co.uk/product/home-care/parkinsons-disease-advanced-practice/'
    },
    {
        title: 'Physical Interventions, conflict management Practical',
        href:
            'https://www.thctc.co.uk/product/care-home/physical-interventions-conflict-management-practical/'
    },
    {
        title: 'Stroke Awareness Advanced',
        href:
            'https://www.thctc.co.uk/product/care-home/stroke-awareness-advanced/'
    },
    {
        title: 'Understanding and observing common ailments in the elderly',
        href:
            'https://www.thctc.co.uk/product/care-home/understanding-and-observing-common-ailments-in-the-elderly/'
    },
    {
        title: 'Substance Use and Abuse ',
        href:
            'https://www.thctc.co.uk/product/care-home/substance-use-and-abuse/'
    },
    {
        title: 'The Accredited Activities Coordinator',
        href:
            'https://www.thctc.co.uk/product/care-home/the-accredited-activities-coordinator/'
    },
    {
        title: 'Understanding Domestic Abuse and Violence ',
        href:
            'https://www.thctc.co.uk/product/care-home/understanding-domestic-abuse-and-violence/'
    },
    {
        title: 'Understanding Forensic Mental health ',
        href:
            'https://www.thctc.co.uk/product/care-home/understanding-forensic-mental-health/'
    },
    {
        title: 'Understanding Person centred Dementia Care Accredited Award ',
        href:
            'https://www.thctc.co.uk/product/care-home/understanding-person-centred-dementia-care-accredited-award/'
    },
    {
        title: 'Personal Care ',
        href: 'https://www.thctc.co.uk/product/home-care/personal-care/'
    },
    {
        title: 'Basic Life Support',
        href: 'https://www.thctc.co.uk/product/home-care/basic-life-support/'
    },
    {
        title: 'First Aid',
        href: 'https://www.thctc.co.uk/product/home-care/first-aid/'
    },
    {
        title: 'Neurological Disorders ',
        href:
            'https://www.thctc.co.uk/product/care-home/neurological-disorders/'
    },
    {
        title: 'Medication and poly pharmacy Advanced ',
        href:
            'https://www.thctc.co.uk/product/care-home/medication-and-poly-pharmacy-advanced/'
    },
    {
        title: 'Managing Dementia Care Advanced ',
        href:
            'https://www.thctc.co.uk/product/care-home/managing-dementia-care-advanced/'
    },
    {
        title: 'Handling Threatening Situations - Housing Officers',
        href:
            'https://www.thctc.co.uk/product/home-care/handling-threatening-situations-housing-officers/'
    },
    {
        title: 'Handling Threatening Phone Calls',
        href:
            'https://www.thctc.co.uk/product/home-care/handling-threatening-phone-calls/'
    },
    {
        title: 'Care Certificate ',
        href: 'https://www.thctc.co.uk/product/home-care/care-certificate/'
    },
    {
        title: 'Augmented communication',
        href:
            'https://www.thctc.co.uk/product/care-home/augmented-communication/'
    },
    {
        title: 'Autism and spectrum disorders ',
        href:
            'https://www.thctc.co.uk/product/care-home/autism-and-spectrum-disorders/'
    },
    {
        title: 'Brain injuries and neurological disorders ',
        href:
            'https://www.thctc.co.uk/product/care-home/brain-injuries-and-neurological-disorders/'
    },
    {
        title: 'Branching out – Aspects of Learning Disabilities Advanced ',
        href:
            'https://www.thctc.co.uk/product/care-home/branching-out-aspects-of-learning-disabilities-advanced/'
    },
    {
        title: 'Clinical Procedures in Practice Instruction',
        href:
            'https://www.thctc.co.uk/product/care-home/clinical-procedures-in-practice-instruction/'
    },
    {
        title: 'Drugs and Legal highs: How to be safe',
        href:
            'https://www.thctc.co.uk/product/care-home/drugs-and-legal-highs-how-to-be-safe/'
    },
    {
        title: 'Epilepsy, Rectal Diazepam and Buccal Midazolam Advanced ',
        href:
            'https://www.thctc.co.uk/product/care-home/epilepsy-rectal-diazepam-and-buccal-midazolam-advanced/'
    },
    {
        title: 'Equality and Inclusion Advanced',
        href:
            'https://www.thctc.co.uk/product/care-home/equality-and-inclusion-advanced/'
    },
    {
        title: 'MCA and DOLS',
        href: 'https://www.thctc.co.uk/product/home-care/mca-and-dols/'
    },
    {
        title:
            'Body language for Managers: How to Read your Employees’ Non-Verbal Signals',
        href:
            'https://www.thctc.co.uk/product/care-home/body-language-for-managers-how-to-read-your-employees-non-verbal-signals/'
    },
    {
        title: 'Health Walk Leader Training Course ',
        href:
            'https://www.thctc.co.uk/product/care-home/health-walk-leader-training-course/'
    },
    {
        title: ' Coaching & supervision',
        href:
            'https://www.thctc.co.uk/product/care-home/coaching-supervision-practical-team-leader-training/'
    },
    {
        title: 'Managing under performing employees',
        href:
            'https://www.thctc.co.uk/product/care-home/managing-under-performing-employees/'
    },
    {
        title: 'Negotiation Skills In The Care Sector',
        href:
            'https://www.thctc.co.uk/product/care-home/negotiation-skills-in-the-care-sector/'
    },
    {
        title: 'Get Ready for Inspection ',
        href:
            'https://www.thctc.co.uk/product/care-home/get-ready-for-inspection/'
    },
    {
        title: 'Diversity and Cultural Competency in Health Care Settings',
        href:
            'https://www.thctc.co.uk/product/care-home/diversity-and-cultural-competency-in-health-care-settings/'
    },
    {
        title: 'Gentle Chair Based Exercise Leader Training',
        href:
            'https://www.thctc.co.uk/product/care-home/gentle-chair-based-exercise-leader-training/'
    },
    {
        title: 'Health Walk Leader Training Course ',
        href:
            'https://www.thctc.co.uk/product/care-home/health-walk-leader-training-course-2/'
    },
    {
        title: 'Parkinson’s Disease: Advanced Practice in Community Care',
        href:
            'https://www.thctc.co.uk/product/care-home/parkinsons-disease-advanced-practice-in-community-care/'
    },
    {
        title: 'Diabetes Awareness',
        href: 'https://www.thctc.co.uk/product/home-care/diabetes-awareness/'
    },
    {
        title: 'Person Centred Activities and Dementia Advanced Practice ',
        href:
            'https://www.thctc.co.uk/product/care-home/person-centred-activities-and-dementia-advanced-practice/'
    },
    {
        title: 'Avoiding Challenging Behaviours in Dementia Care',
        href:
            'https://www.thctc.co.uk/product/home-care/avoiding-challenging-behaviours-in-dementia-care/'
    }
];

module.exports = careHomesLinks;
