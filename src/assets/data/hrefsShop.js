const hrefsShop = [
    'https://www.thctc.co.uk/product/shop/chair-based-exercise-in-care-and-nursing-homes/',
    'https://www.thctc.co.uk/product/do-it-yourself-stroke-recovery-planning-kit/',
    'https://www.thctc.co.uk/product/falls-prevention/',
    'https://www.thctc.co.uk/product/management-of-back-pain/',
    'https://www.thctc.co.uk/product/managing-back-pain-the-dvd/',
    'https://www.thctc.co.uk/product/person-centred-activities-dementia/',
    'https://www.thctc.co.uk/product/person-centred-activities-and-dementia-2/',
    'https://www.thctc.co.uk/product/recovery-after-stroke/',
    'https://www.thctc.co.uk/product/recovery-after-stroke-the-exercise-dvd/',
    'https://www.thctc.co.uk/product/seated-exercise-for-residential-and-nursing-homes/',
    'https://www.thctc.co.uk/product/shop/seated-exercise-for-residential-and-nursing-homes-the-exercise-dvd/',
    'https://www.thctc.co.uk/product/the-tales-of-a-stroke-patient-by-joyce-hoffman/'
];
module.exports = hrefsShop;
