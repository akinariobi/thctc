const shopItems = [
    {
        title:
            'Seated exercise for residential and nursing homes: The exercise DVD',
        price: '20.00',
        'description-p': [
            undefined,
            'There are many misconceptions around the place of physical activity, later life and seated exercise training. The aim of this instructional DVD is to make up for any gaps in activity coordinator training in a simple and straight forward way. The exercises in this programme have been developed by George Flaherty over the last fifteen years whilst working in older peoples care in nursing and residential homes. The exercises are designed to be part of the complete therapeutic activities program, complimenting the prevention of falls, reminiscence therapy, and creative activities for the elderly.'
        ],
        'description-ol': []
    },
    {
        title: 'Recovery after stroke: The exercise DVD',
        price: '15.00',
        'description-p': [
            undefined,
            'A stroke is a life changing event, not only for those who have one, but the people around them. The sound advice offered by George Flaherty is based on nearly twenty years helping stroke survivors with his own brand of stroke rehab exercises. These exercises are designed to take up where occupational therapy and physio rehab for stroke finishes. The exercises are targeted at improving stroke symptoms and long term stroke recovery.'
        ],
        'description-ol': []
    },
    {
        title: 'Falls prevention',
        price: '15.00',
        'description-p': [
            undefined,
            'Reducing the risk of falling. The DVD is instructional and helps the user implement a programme to reduce the number of falls experienced by those in social care setting. The exercises are based on dynamic balance, conditioning, stretching and strengthening. The sound advice offered by George Flaherty is based on nearly twenty years helping older people maintain or improve their mobility. The exercises are designed specifically for older people in social care setting.'
        ],
        'description-ol': []
    },
    {
        title: 'Managing back pain: The DVD',
        price: '15.00',
        'description-p': [
            undefined,
            'Back pain problems are among the most common causes of distress and discomfort in modern western society today. The sound advice offered by George Flaherty, known affectionately as “the back doctor” by many of those he has helped over the year, makes this one of the best exercise DVDs on back pain relief on the market. This has made it rated as one top exercise DVDs by users. The exercise based rehabilitation programme shown is designed to offer back care solutions for shoulder neck pain, upper back pain, slipped disk symptoms and sciatica leg pain.'
        ],
        'description-ol': []
    },
    {
        title: 'Recovery after stroke',
        price: '15.00',
        'description-p': [
            undefined,
            'This self help book takes up where occupational, physio therapy and NHS stroke rehabilitation leaves off. Offering advice on stroke rehab exercises for stroke survivors. The advice offered by George Flaherty is based on nearly twenty years of experience helping those who have had a stroke, with his own brand of stroke rehab exercises. These have been tried, tested and proven to help improve stroke symptoms and more importantly stroke recovery. The written version of “Recovery after stroke” is complimented by the exercise DVD of the same name.'
        ],
        'description-ol': []
    },
    {
        title: 'Management of back pain',
        price: '15.00',
        'description-p': [
            undefined,
            'This book takes up where occupational and physio therapy leaves off in chronic pain management related to bad back problems. The text looks at a range back pain symptoms including sciatica pain, shoulder neck pain and slipped disk symptoms. The author, George Flaherty, is known by those whom he has worked with as a back pain specialist although equally well known for his rehabilitation exercises for chronic illness management. The book is complimented by the DVD.'
        ],
        'description-ol': []
    },
    {
        title: 'Person centred activities and dementia',
        price: '15.00',
        'description-p': [
            undefined,
            'This book focuses on the role of person centred assessment for activity co-ordinators working in, residential care homes and nursing home care. The explanation of the person centred assessment leads on to an outline of a range of therapeutic activity ideas for arts and crafts, creative activities. Physical activity based on current activity theory for dementia including Alzheimer’s dementia, vascular dementia and Lewy bodies to name a few.',
            'The author George Flaherty has over fifteen years experience working in older peoples care in nursing and residential homes, He has created the ideas contained within the guide to compliment a complete therapeutic activities program.'
        ],
        'description-ol': []
    },
    {
        title: 'Do It Yourself Stroke Recovery Planning Kit',
        price: '15.00',
        'description-p': [
            'Stroke recovery is about getting your life back together after having the equivalent of a nuclear explosion hit your brain. This simple realisation of the effect of a stroke was the starting point of the concept of Stroke Recovery Planning.',
            'As part of Stroke Recovery Planning, although we do work on them every day, getting the use of an arm back or achieving the perfect walking pattern isn’t the ultimate aim. The aim is to make you feel whole again. To be able to laugh whilst you’re playing with the kids, spend meaningful time with people you care about. Accept the stroke as a life event, not something that defines who you are as a person.',
            'In this light a good Stroke Recovery Plan deals with the general effects of a stroke. A great recovery plan deals with the aftermath of the stroke had on complete existence. It explores your emotional state, your understanding of the effects of the stroke, helps you set realistic expectations for your recovery. Maintain a helpful mind set to your recovery. It identifies ways to bring you through the loss of your old self and improves the ability and quality of care and the support available from family. It creates a high quality daily, weekly, monthly, yearly schedule of rounded activity aimed at reaching equally high quality personal recovery goals.'
        ],
        'description-ol': []
    },
    {
        title: 'Chair Based Exercise in Care and Nursing Homes',
        price: '27.00',
        'description-p': [
            undefined,
            undefined,
            'Chair based exercise is the integral part activities in the care home settings. This training manual offers an insight to effective chair based exercise for older people living in care.',
            undefined,
            'By the end of reading this manual the reader should have a basic understanding of the management and delivery of chair based exercise in care settings. This manual is meant to be used as part of a tutor based training session.',
            undefined,
            'Those who read the manual will help the reader identify:',
            ' Google Code for training courses conversions Conversion Page '
        ],
        'description-ol': [
            'The changes in physical function brought on by age',
            'The basic structure of a chair based exercise session',
            'The component parts within a chair based exercise session',
            'A range of equipment recommended used in a session',
            'The risk assessment measures that need to be taken',
            'The reasons why someone can’t be included in an exercise session'
        ]
    },
    {
        title: 'Seated exercise for residential and nursing homes',
        price: '15.00',
        'description-p': [
            undefined,
            'There are many misconceptions around the place of physical activity, later life and seated exercise training. The aim of this book is to make up for any gaps in activity coordinator training in a simple and straight forward way. The exercises in this programme have been developed by George Flaherty over the last fifteen years whilst working in older peoples care in nursing and residential homes. The exercises are designed to be part of the complete therapeutic activities program, complimenting the prevention of falls, reminiscence therapy, and creative activities for the elderly. The written version of Seated Exercise for Residential and Nursing Homes is complimented by the exercise DVD of the same name.'
        ],
        'description-ol': []
    },
    {
        title: 'The Tales of a Stroke Patient * by Joyce Hoffman',
        price: '12.55',
        'description-p': [
            "Cardiovascular accidents, commonly known as strokes, account for about 800,000 annual cases worldwide. Strokes are not only a leading cause of death, they are also the major cause of serious, long-term disability. Joyce Hoffman recalls her own experience as a stroke survivor from the patient's point of view, and her ongoing struggle to recover from disability, in The Tales of a Stroke Patient.",
            'Hoffman begins with the symptoms that preceded the sudden and unexpected stroke that left her paralyzed and unable to speak. She then recollects her time in the hospital, the hopelessness, fear and frustration she felt, and the slow journey towards recovery she had to undertake. Hoffman recounts her difficulties and misunderstandings with the overworked staff of her rehabilitation center, the long hours of therapy she had to endure, and the progress she made, albeit slow, despite her disabilities. In her book, Hoffman also shares important medical advice, as well as a variety of other stories from her life. ',
            'With The Tales of a Stroke Patient, Hoffman hopes to help other stroke survivors re-gain the dignity, self esteem, and empowerment that was taken from them. Her work is a sincere depiction of the disabilities and difficulties countless people have to live with on a daily basis, and serves to raise awareness on one of the most important health issues today. The Tales of a Stroke Patient ',
            "Hoffman's book is available online in paperback or e-book format from Amazon, Xlibris, Barnes and Noble, and from online booksellers almost everywhere. If you don't have a credit or debit card, she can send one to you directly if you write her an email for instructions at hcwriter@gmail.com.",
            'About the Author',
            'Publication Date: 09/26/2012'
        ],
        'description-ol': []
    },
    {
        title: 'Person centred activities & dementia',
        price: '15.00',
        'description-p': [
            undefined,
            'George talks about the role of person centred assessment for activity co-coordinators working in, residential care homes and nursing home care. He shows how activities can be adapted and outlines a range of different activity ideas. George Flaherty has over fifteen years experience working in older peoples care in nursing and residential homes. He has created the ideas contained within the guide to compliment a complete therapeutic activities programme.'
        ],
        'description-ol': []
    }
];

export default shopItems;
