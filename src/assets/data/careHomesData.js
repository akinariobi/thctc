const careHomesData = [
    {
        title: 'Massage Therapy for Older People',
        price: '80.00',
        'description-p': [
            'Open course on 7th May 2019 in Essex. Places are limited. Cost is £80 per person',
            'A practical course that helps activity co-ordinators provide safe and effective massage to residents with a range of chronic conditions.  This course shows you how to create, adapt and deliver a range of safe massages for older people living in care settings.  All our courses are very practical, and you’ll learn techniques that are easy to apply on the care floor.',
            'By the end of the course delegates will have the knowledge to understand:',
            'A certificate is provide upon completion of the course.'
        ],
        'description-ol': [
            'Things you need to think about when massaging older people. The affect massage has on improving mobility, pain and quality of life.',
            'Health and safety considerations required to keep the resident, masseur and organisation safe',
            'Has sessions practicing a complete range of safe, easy massage techniques that can be used in care settings',
            'Shows you how to create your own safe programmes from scratch quickly',
            'How to create and adapt massages for:\n',
            'Neck pain, Stiff shoulders, Painful elbows. and fingers, Lower back pain, and Painful calf, feet and toes'
        ]
    },
    {
        title: 'Managing Dementia Care Advanced',
        price: '',
        'description-p': [
            'This accredited dementia care course contributes toward the recommended guided learning hours. you should carry out practice reflection, document and attach subsequent evidence from this training. The course involves use of the SFC knowledge sets for Dementia Care and distance learning dementia care workbook. The course will provide for care managers and key senior care staff an advanced benchmark standard of Dementia care. Delivery methods will include reflective practice, intervention tools, group work, case studies and visual presentation.'
        ],
        'description-ol': [
            'Understand where dementia care fits in to the Qualification and credit framework and the role of the manager',
            'Understand the legal aspects of dementia care',
            'Equality and inclusion issues when planning dementia care',
            'Understand the importance of working in a person centered way and how it links to inclusion',
            'Person centered care planning for dementia care',
            'Involving the SU, family and multi disciplinary team in the care plan process'
        ]
    },
    {
        title: 'Communication and Brain Injury',
        price: '',
        'description-p': [
            'This course will help those working with people with a brain injury to support their comprehension and expression using different communication strategies. This course is run by a qualified Speech and Language Therapist who is registered with the Health and Care Professions Council (HCPC).',
            'By the end of the day those who attend will be able to: communicate more effectively with people with a brain injury.',
            'Those who attend the course will have a better understanding of:'
        ],
        'description-ol': [
            'Language Impairment and Brain Injury',
            'Cognitive Communication Difficulties and Brain Injury.',
            'Communicating with People with a Brain Injury.',
            'How to Help Communication',
            'Using Low-tech and Hi-tech Communication Strategies'
        ]
    },
    {
        title: 'Falls Prevention Advanced Practice Train the trainer',
        price: '',
        'description-p': [
            'The Falls Prevention Champions Course is unique. Its a mix of exercises to help older people stay healthy, fit and standing. The skills needed for your team members to run these classes for service users safely. To do falls risk assessments. Prepare and run one day falls prevention courses for other staff members.',
            'By the end of the day students will be able to:'
        ],
        'description-ol': [
            'Identify with what it feels like to be old and frighten of falling',
            'Emotionally understand the fear and impact of falls on a persons confidence',
            'Identify good bone health, osteoporosis and reducing fracture risk',
            'Identify and actively minimise risk factors connected to falls',
            'Conduct an individual and generic falls risk assessments'
        ]
    },
    {
        title: 'Parkinson’s Disease: Advanced Practice in Community Care',
        price: '',
        'description-p': [
            'The course really is something very special, bespoke and unique. It offers a very in-depth package of study on and around the support and management of Parkinson’s in the community. As an organisation we are proud to deliver this package.',
            'The course will provide those who attend an opportunity for an in-depth exploration of Parkinson’s Disease and how to effectively support this in community care settings.',
            'By the end of your studies you’ll be able to identify:'
        ],
        'description-ol': [
            'The realities from a personal and professional point of view',
            'The effects of the most common medications used for the management of Parkinson’s disease in the UK',
            'Alternative therapies and the management of Parkinson’s disease',
            'Exercise management and prescription of Parkinson’s disease',
            'How all these elements come together in the effective support and management of Parkinson’s in the community'
        ]
    },
    {
        title: 'Falls Prevention Advanced Practice',
        price: '',
        'description-p': [
            'In short this course shows your team how to throw everything but the sink at preventing falls on your care sites.',
            'The Falls Prevention Champions Course is unique. Its a mix of exercises to help older people stay healthy, fit and standing. The skills needed for your team members to run these classes for service users safely. To do falls risk assessments. Prepare and run one day falls prevention courses for other staff members.',
            'The aim of the course to help students to develop the skills to effectively lead the prevention of trips, slips and falls in their work place.',
            'By the end of the day students will be able to:'
        ],
        'description-ol': [
            'Identify with what it feels like to be old and frighten of falling',
            'Emotionally understand the fear and impact of falls on a persons confidence',
            'Identify good bone health, osteoporosis and reducing fracture risk',
            'Identify and actively minimise risk factors connected to falls',
            'Conduct an individual and generic falls risk assessments',
            'How to respond appropriately to a fall',
            'Use the Queensland Stay on Your Feet Toolkit',
            'Assess service users suitability for a falls prevention exercise class'
        ]
    },
    {
        title: 'Person Centred Activities and Dementia Advanced Practice',
        price: '',
        'description-p': [
            'The aim of this advanced course is to equip those who attend with the skills to assess, plan and deliver person centered activities to a range of residents with a variety of dementia types and stages to an expert level.',
            'By the end of the course students will be able to:',
            'Students have to complete a three part assessment process:',
            ' '
        ],
        'description-ol': [
            'Assess the residents activity needs in a way suitable to their service',
            'Develop an person centered activity plan',
            'Effortlessly access an endless stream of impressive activity ideas',
            'Adapt programs to the different stages and types of dementia',
            'Adapt programs to medical needs of older people',
            'Evaluate the effectiveness of programs',
            'Critically reflect on practice',
            'Prepare a twelve week person centered activity plan for a selected resident'
        ]
    },

    {
        title: 'Professional Boundaries in Care',
        price: '',
        'description-p': [],
        'description-ol': [
            'Understand what professional boundaries are',
            'Understand why professional boundaries are important',
            'Understand what are some potential consequences of a service provider having poor professional boundaries',
            'Develop techniques for creating & maintaining boundaries'
        ]
    },
    {
        title: 'Introduction to Autism',
        price: '',
        'description-p': [
            'This course will help parents and carers to gain a greater understanding of Autism. This course is run by an experienced Speech and Language Therapist who is registered with the Health and Care Professions Council (HCPC).',
            'Those who attend the course will have a better understanding of:'
        ],
        'description-ol': [
            'What is Autism?',
            'Myth busting',
            'Triad of impairments',
            'Asperger’s Syndrome'
        ]
    },
    {
        title: 'HIV Awareness',
        price: '',
        'description-p': [
            'Overview',
            'By the end of the course students will be able to:',
            'Delegates will be issued with certificates upon completion. Cost (Incl VAT and expenses) £275.00. Enquire now using the form on the left.'
        ],
        'description-ol': [
            'Understand what is HIV',
            'How common HIV is',
            'How it is transmitted and prevented'
        ]
    },
    {
        title: 'Activities for those in the late stages of Dementia',
        price: '80.00',
        'description-p': [
            'Course running on 10th May 2019 in Essex. Places are limited. Cost is £80 per person',
            'This course shows you how to create, adapt and deliver a range of activities for those in the final stages of dementia.  However, the ideas and practical skills learned are equally applicable for anyone with any condition at end of life living in care settings.  All our courses are very practical, and you’ll learn techniques that are easy to apply on the care floor.',
            'By the end of the course delegates will have the knowledge to understand:',
            'A certificate is issued upon completion of the course.'
        ],
        'description-ol': [
            'The things you need to think about when providing activities for those in the final stages of dementia',
            'The creation of a suitable area to provide activities for those in the final stages of dementia. How to create feelings of safety and security.',
            'Has sessions practicing a complete range of safe, effective techniques that can be used in care settings',
            'Shows you how to create your own person-centred programmes from scratch quickly'
        ]
    },
    {
        title: 'Communication and Brain Injury',
        price: '',
        'description-p': [
            'This course will help those working with people with a brain injury to support their comprehension and expression using different communication strategies. This course is run by a qualified Speech and Language Therapist who is registered with the Health and Care Professions Council (HCPC).',
            'By the end of the day those who attend will be able to: communicate more effectively with people with a brain injury.',
            'Those who attend the course will have a better understanding of:'
        ],
        'description-ol': [
            'Language Impairment and Brain Injury',
            'Cognitive Communication Difficulties and Brain Injury.',
            'Communicating with People with a Brain Injury.',
            'How to Help Communication',
            'Using Low-tech and Hi-tech Communication Strategies'
        ]
    },
    {
        title: 'Reablement Awareness',
        price: '',
        'description-p': [
            'For operational based staff, having knowledge of how reablement works, is approached and delivered on the ground offers many benefits. It allows clarity in assessing functional ability to undertake ADLs and IADLS. Monitoring and measuring the recovery achieved.',
            'Aim',
            'The aim of this course is to increase the understanding of how reablement impacts care practice, as operational staff begin to provide packages.',
            'Objectives',
            'Those who attend the course will be able to understand:',
            ' '
        ],
        'description-ol': [
            'How reablement works',
            'The effects of person centred reablement on regaining independence',
            'How to support individuals to learn or relearn, to improve and to recover their abilities',
            'Identify the journey from referral and through the process of reablement',
            'Roles and responsibilities of all service staff',
            'Awareness of the potential for evidence-based risk taking and how it should be managed'
        ]
    },
    {
        title:
            'Filling Beds – Practical marketing and sales for care home managers',
        price: '',
        'description-p': [
            'The course will give those who attend the practical skills to maximise occupancy and profit for individual units and the organisation as a whole.',
            'Upon completion students will have a greater:',
            ' '
        ],
        'description-ol': [
            'Understanding the roles of marketing and sales',
            'Identifying which mediums to use for the care sector',
            'Identifying potential new markets',
            'Creating copy that sells',
            'Monitoring copy'
        ]
    },
    {
        title: 'Memory Training',
        price: '',
        'description-p': [
            'What are Memory Training Sessions',
            'The aim of the course is teach those who attend the skills needed to assess, prepare, deliver an evaluate memory training sessions for older people.',
            'By the end of the course students will be able to:'
        ],
        'description-ol': [
            'Assess the needs of their service users with service appropriate tools',
            'Prepare and plan one to one and group memory training sessions',
            'Facilitate and deliver memory training sessions',
            'Identify partial and full exclusions to inclusion in memory training sessions',
            'Be able to critically reflect on their own practice'
        ]
    },
    {
        title: 'Train the trainer – safeguarding adults',
        price: '',
        'description-p': [
            'Over five days those who attend will learn an in depth knowledge of safeguarding and the issues that surround it. How to run in house safe guarding sessions and updates for staff. Finally how to act as site champions for the safeguarding of those who access your services.',

            'The aim of the course is give those who attend the skills to lead and facilitate the training of safeguarding for those in their care, while maintaining the humanity and empathy in the care offered.',

            'By the end of the course those who attend will be able to:',

            'The assessment comprises three areas:',

            'The course is a very intensive and fun weeks training. There is a homework element each night and each day is a very full on experience.'
        ],
        'description-ol': [
            'Define the dignity challenges are their role in care',
            'Define the what abuse is and the common types',
            'Identify the warning signs and symptoms of abuse',
            'Identify the triggers of abuse',
            'Identify their professional responsibility and how to manage disclosure',
            'Identify Where to get help',
            'How and when to whistle blow',
            'Assess staff performance, needs and ability',
            'Identify the different types learning styles and the people learn',
            'Plan organisationally relevant training sessions',
            'Use a variety of training media and presentation techniques',
            'Identify how to communicate effectively in training sessions',
            'Manage and direct students'
        ]
    },
    {
        title: 'Moving and Handling Train the Trainer',
        price: '',
        'description-p': [
            'The aim of the course is to teach students the skills needed to train, coach and supervise person centered moving and handling of patients.',
            'Practically Based Learning Outcomes',
            'Theory Based Learning Outcomes',
            'The assessment comprises three areas:',
            'The course is a very intensive and fun weeks training. There is a homework element each night and each day is a very full on experience. The course offers a lot of hands on in two important subject areas:',
            'However all that attend need to a have a current moving and handling of patients qualification and a least three years relevant handling experience in the care sector.'
        ],
        'description-ol': [
            'Informally assess a residents physical and mental capability',
            'Help a resident stand from a chair and bed',
            'Help a resident transfer safely',
            'Assist a resident to walk a short distance',
            'Help a resident move in bed with equipment',
            'Turn a resident in bed',
            'Slide a resident in bed',
            'Use a variety of pieces of small handling equipment',
            'Hoist a resident form bed to chair and chair to bed',
            'Handling of objects',
            'Informatively and summatively assess students',
            'Research and plan handling training sessions',
            'Create relevant learning materials and resources'
        ]
    },
    {
        title: 'Nurse Verification of Expected Death',
        price: '',
        'description-p': [
            'This course is suitable for nurses who are interested in taking on the extended role of verification of expected death. It is aimed at staff who wish to develop knowledge and skills in recognising death and the process of verifying an expected death.',
            'Historically medical practitioners would verify expected death, however it is acknowledged by the NMC as an extended role.  Therefore registered nurses who have attended training and have been assessed to carry out the role competently can do so, in certain circumstances and in accordance with locally policy.',

            'One day',
            'By the end of the course the candidates will be able to identify:',
            'Delegates will be issued with certificates of attendance valid for a period of 1 year.',
            '£125 per person.',
            'Call 01245 357132 or email sales@thctc.co.uk to book your place.'
        ],
        'description-ol': [
            'What constitutes an “expected death”',
            'Recognise the clinical signs of death',
            'Perform a relevant clinical examination',
            'Terminology including certification, verification and expected death',
            'Diagnosis and recognition of death',
            'Communication and difficult conversations',
            'Understanding of the legal and professional framework in relation to the verification of expectant death',
            'The role of the Coroner and the related legal issues',
            'Person centred care',
            'Recording the fact of death accurately',
            'Evidence of competency.'
        ]
    }
];

export default careHomesData;
