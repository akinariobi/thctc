const homeCareLinks = [
    {
        title: ' Dignity in Care Champions Training',
        href:
            'https://www.thctc.co.uk/product/home-care/dignity-in-care-champions-training/'
    },
    {
        title: ' Infection control ',
        href: 'https://www.thctc.co.uk/product/home-care/infection-control/'
    },
    {
        title: ' Risk assessment for community based care workers ',
        href:
            'https://www.thctc.co.uk/product/home-care/risk-assessment-for-community-based-care-workers/'
    },
    {
        title: 'Avoiding Challenging Behaviours in Dementia Care',
        href:
            'https://www.thctc.co.uk/product/home-care/avoiding-challenging-behaviours-in-dementia-care/'
    },
    {
        title: 'Basic Life Support',
        href: 'https://www.thctc.co.uk/product/home-care/basic-life-support/'
    },
    {
        title: 'Care Certificate ',
        href: 'https://www.thctc.co.uk/product/home-care/care-certificate/'
    },
    {
        title: 'Caring for People at Home ',
        href:
            'https://www.thctc.co.uk/product/home-care/caring-for-people-at-home/'
    },
    {
        title: 'Coaching and Supervision ',
        href:
            'https://www.thctc.co.uk/product/home-care/coaching-and-supervision/'
    },
    {
        title: 'Communication and Autism',
        href:
            'https://www.thctc.co.uk/product/home-care/communication-and-autism-2/'
    },
    {
        title: 'Communication and Dementia ',
        href:
            'https://www.thctc.co.uk/product/home-care/communication-and-dementia/'
    },
    {
        title: 'Dementia Awareness for Home Care ',
        href:
            'https://www.thctc.co.uk/product/home-care/dementia-awareness-for-home-care/'
    },
    {
        title: 'Diabetes Awareness',
        href: 'https://www.thctc.co.uk/product/home-care/diabetes-awareness/'
    },
    {
        title: 'Dignity in Care ',
        href: 'https://www.thctc.co.uk/product/home-care/dignity-in-care/'
    },
    {
        title: 'Emergency first aid awareness  ',
        href:
            'https://www.thctc.co.uk/product/home-care/emergency-first-aid-awareness/'
    },
    {
        title: 'Enablement of Older People Living Independently ',
        href:
            'https://www.thctc.co.uk/product/home-care/enablement-of-older-people-living-independently/'
    },
    {
        title: 'Equality and diversity  ',
        href:
            'https://www.thctc.co.uk/product/home-care/equality-and-diversity/'
    },
    {
        title: 'Fire safety awareness',
        href: 'https://www.thctc.co.uk/product/home-care/fire-safety-awareness/'
    },
    {
        title: 'First Aid',
        href: 'https://www.thctc.co.uk/product/home-care/first-aid/'
    },
    {
        title: 'Food safety awareness',
        href: 'https://www.thctc.co.uk/product/home-care/food-safety-awareness/'
    },
    {
        title: 'Getting staff to attend training ',
        href:
            'https://www.thctc.co.uk/product/home-care/getting-staff-to-attend-training/'
    },
    {
        title: 'Handling Threatening Phone Calls',
        href:
            'https://www.thctc.co.uk/product/home-care/handling-threatening-phone-calls/'
    },
    {
        title: 'Handling Threatening Situations ',
        href:
            'https://www.thctc.co.uk/product/home-care/handling-threatening-situations/'
    },
    {
        title: 'Handling Threatening Situations - Housing Officers',
        href:
            'https://www.thctc.co.uk/product/home-care/handling-threatening-situations-housing-officers/'
    },
    {
        title: 'Health and safety awareness',
        href:
            'https://www.thctc.co.uk/product/home-care/health-and-safety-awareness/'
    },
    {
        title: 'HIV Awareness',
        href: 'https://www.thctc.co.uk/product/home-care/hiv-awareness/'
    },
    {
        title: 'Introduction to Autism',
        href:
            'https://www.thctc.co.uk/product/home-care/introduction-to-autism/'
    },
    {
        title: 'Introduction to Learning Disability',
        href:
            'https://www.thctc.co.uk/product/home-care/introduction-to-learning-disability/'
    },
    {
        title: 'Lone Working ',
        href: 'https://www.thctc.co.uk/product/home-care/lone-working/'
    },
    {
        title: 'Managing difficult staff',
        href:
            'https://www.thctc.co.uk/product/home-care/managing-difficult-staff/'
    },
    {
        title: 'Mandatory Awareness Courses ',
        href:
            'https://www.thctc.co.uk/product/home-care/mandatory-awareness-courses/'
    },
    {
        title: 'Manual Handling of objects',
        href:
            'https://www.thctc.co.uk/product/home-care/manual-handling-of-objects/'
    },
    {
        title: 'MCA and DOLS',
        href: 'https://www.thctc.co.uk/product/home-care/mca-and-dols/'
    },
    {
        title: 'Medications awareness for care',
        href:
            'https://www.thctc.co.uk/product/home-care/medications-awareness-for-care/'
    },
    {
        title: 'Memory Training',
        href: 'https://www.thctc.co.uk/product/home-care/memory-training/'
    },
    {
        title: 'Moving & handling people with behaviours that challenge',
        href:
            'https://www.thctc.co.uk/product/home-care/moving-handling-people-with-behaviours-that-challenge/'
    },
    {
        title: 'Moving and handling - Refresher Training',
        href:
            'https://www.thctc.co.uk/product/home-care/moving-and-handling-refresher-training/'
    },
    {
        title: 'Moving and Handling Train the Trainer    ',
        href:
            'https://www.thctc.co.uk/product/home-care/moving-and-handling-train-the-trainer/'
    },
    {
        title: "Parkinson's Awareness",
        href: 'https://www.thctc.co.uk/product/home-care/parkinsons-awareness/'
    },
    {
        title: 'Parkinson’s Disease: Advanced Practice',
        href:
            'https://www.thctc.co.uk/product/home-care/parkinsons-disease-advanced-practice/'
    },
    {
        title: 'Person Centred Care ',
        href: 'https://www.thctc.co.uk/product/home-care/person-centred-care/'
    },
    {
        title: 'Personal Care ',
        href: 'https://www.thctc.co.uk/product/home-care/personal-care/'
    },
    {
        title: 'Pressure sore care, prevention and awareness',
        href:
            'https://www.thctc.co.uk/product/home-care/pressure-sore-care-prevention-and-awareness/'
    },
    {
        title: 'Professional Boundaries in Care',
        href:
            'https://www.thctc.co.uk/product/home-care/professional-boundaries-in-care-2/'
    },
    {
        title: 'Reablement Awareness',
        href: 'https://www.thctc.co.uk/product/home-care/reablement-awareness/'
    },
    {
        title: 'Reablement Train the Trainer',
        href:
            'https://www.thctc.co.uk/product/home-care/reablement-train-the-trainer/'
    },
    {
        title: 'Safeguarding of Vulnerable Adults     ',
        href:
            'https://www.thctc.co.uk/product/home-care/safeguarding-of-vulnerable-adults/'
    },
    {
        title: 'Social Care Train the Trainer',
        href:
            'https://www.thctc.co.uk/product/home-care/social-care-train-the-trainer/'
    },
    {
        title: 'Supporting those with Depression',
        href:
            'https://www.thctc.co.uk/product/home-care/supporting-those-with-depression/'
    },
    {
        title: 'Train the Trainer',
        href: 'https://www.thctc.co.uk/product/home-care/train-the-trainer/'
    },
    {
        title: 'Train the trainer lone working',
        href:
            'https://www.thctc.co.uk/product/home-care/train-the-trainer-lone-working/'
    },
    {
        title: 'Train the trainer prevention and control of infection',
        href:
            'https://www.thctc.co.uk/product/home-care/train-the-trainer-prevention-and-control-of-infection/'
    },
    {
        title: 'Train the trainer safeguarding vulnerable adults',
        href:
            'https://www.thctc.co.uk/product/home-care/train-the-trainer-safeguarding-vulnerable-adults/'
    }
];

module.exports = homeCareLinks;
