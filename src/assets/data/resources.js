const resources = [
    {
        title:
            'Wellbeing in Dementia: An Occupational Approach for Therapists and Carers',
        author: 'Tessa Perrin & Hazel May',
        year: '1999',
        href:
            'https://www.amazon.co.uk/Wellbeing-Dementia-Occupational-Therapists-Paperback/dp/B00JYI0N4W/ref=sr_1_1?s=books&ie=UTF8&qid=1500368392&sr=1-1&keywords=Perrin+and+May'
    },
    {
        title:
            'The Trigger Point Therapy Workbook: Your Self -Treatment Guide for Pain Relief: Your Self-Treatment for Pain Relief',
        author: 'Clair Davies',
        year: '2013',
        href:
            'https://www.amazon.co.uk/Trigger-Point-Therapy-Workbook-Self-Treatment/dp/1608824942/ref=dp_ob_title_bk'
    },
    {
        title: 'The End-of-Life Namaste Care Program for People with Dementia',
        author: 'Joyce Simard',
        year: '2013',
        href:
            'https://www.amazon.co.uk/d/Books/End-Life-Namaste-Care-Program-People-Dementia/1938870026/ref=sr_1_1?s=books&ie=UTF8&qid=1500369799&sr=1-1&keywords=namaste+care'
    },
    {
        title:
            'The Barbell Prescription: Strength Training for Life After 40 Paperback',
        author: 'Jonathon M Sullivan',
        year: '2016',
        href:
            'https://www.amazon.co.uk/gp/product/0982522770/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1'
    },
    {
        title: 'Challenging Behaviour in Dementia: A Person-Centred Approach',
        author: 'Graham Stokes',
        year: '2000',
        href:
            'https://www.amazon.co.uk/Challenging-Behaviour-Dementia-Person-Centred-Speechmark/dp/0863883974/ref=sr_1_3?s=books&ie=UTF8&qid=1500465277&sr=1-3&keywords=Graham+stokes'
    },
    {
        title:
            'Dementia: The One-Stop Guide: Practical advice for families, professionals, and people living with dementia and Alzheimer’s Disease',
        author: 'June Andrews',
        year: '2015',
        href:
            'https://www.amazon.co.uk/Dementia-One-Stop-Practical-professionals-Alzheimers/dp/1781251711'
    }
];

export default resources;
