const homeCareData = [
    {
        title: 'Introduction to Autism',
        price: '',
        'description-p': [
            'This course will help parents and carers to gain a greater understanding of Autism. This course is run by an experienced Speech and Language Therapist who is registered with the Health and Care Professions Council (HCPC).',
            'By the end of the day those who attend will have a greater understanding of Autism including:'
        ],
        'description-ol': [
            'What is Autism?',
            'Myth busting',
            'Triad of impairments',
            'Asperger’s Syndrome'
        ]
    },
    {
        title: 'Manual Handling of objects',
        price: '',
        'description-p': [
            "Moving and Handling is unique to each job, place and person. So is this course. If you want to improve your teams performance and want something something that's in plain English call. If you want another PowerPoint day click on the guys above or below on listing page.",
            'This course offers the practical information and skills needed to handle objects safely at work.',

            'By the end of the day those who attend will be able to:'
        ],
        'description-ol': [
            'Identify relevant legislation',
            'Conduct informal and formal risk assessments',
            'Identify and maintain a safer lifting posture',
            'The limits of individual capability',
            'Problem solving & practical handling of inanimate loads',
            'Team handling of inanimate objects'
        ]
    },
    {
        title: 'Person Centred Care',
        price: '',
        'description-p': [
            'Participants will explore the meaning of true person-centred care and examine methods, skills and tools for achieving it.  The course aims to enable care staff to understand and improve on the level of person-centred care they are able to provide to their clients.',
            'By the end of the course the delegates will have an understanding of:'
        ],
        'description-ol': [
            'The concept of person centred care',
            'The care continuum and changing needs of those in care',
            'Understand how to create and maintain a person-centred approach to the care',
            'Confidently apply the core principles of care planning'
        ]
    },
    {
        title: 'Reablement Train the Trainer',
        price: '',
        'description-p': [
            'Reablement Train the Trainer',
            'This course is designed to give organisations the ability to develop their own training team to cascade learning down to operational staff.  The skills learned will be based on the delivery of reablement training in care practice.',
            'Objectives',
            'Those who attend the course will be able to understand:',
            'Duration',
            '3 days'
        ],
        'description-ol': [
            'How to construct a reablement training course',
            'Deliver a reablement course',
            'The effects of person centred reablement on regaining independence',
            'How to support individuals to learn or relearn, to improve and to recover their abilities',
            'Identify the journey from referral and through the process of reablement',
            'Roles and responsibilities of all service staff'
        ]
    },
    {
        title: 'Coaching and Supervision',
        price: '',
        'description-p': [
            'The aim of this course is to offer team leaders the chance to access and practice the practical skills needed to manage and lead a team of people successfully in a safe environment.',
            'By the end of the day those who will be able to identify:',
            ' '
        ],
        'description-ol': [
            'The many hats of a team leader (Skills and abilities)',
            'How to manage and lead others',
            'Motivating performers and under performers',
            'What work based communication is and how to use it to manage and lead teams',
            'Practical techniques to deal with difficult working relationships',
            'Day to day problem solving and critical reflection'
        ]
    },
    {
        title: 'Moving and Handling Train the Trainer',
        price: '',
        'description-p': [
            'The aim of the course is to teach students the skills needed to train, coach and supervise person centered moving and handling of patients.',
            'Practically Based Learning Outcomes',
            'Theory Based Learning Outcomes',

            'The assessment comprises three areas:',

            'The course is a very intensive and fun weeks training. There is a homework element each night and each day is a very full on experience. The course offers a lot of hands on in two important subject areas:',
            'However all that attend need to a have a current moving and handling of patients qualification and a least three years relevant handling experience in the care sector.'
        ],
        'description-ol': [
            'Informally assess a residents physical and mental capability',
            'Help a resident stand from a chair and bed',
            'Help a resident transfer safely',
            'Assist a resident to walk a short distance',
            'Help a resident move in bed with equipment',
            'Turn a resident in bed',
            'Slide a resident in bed',
            'Use a variety of pieces of small handling equipment',
            'Hoist a resident form bed to chair and chair to bed',
            'Handling of objects',
            'Informatively and summatively assess students',
            'Research and plan handling training sessions',
            'Create relevant learning materials and resources'
        ]
    },
    {
        title: 'Reablement Awareness',
        price: '',
        'description-p': [
            'For operational based staff, having knowledge of how reablement works, is approached and delivered on the ground offers many benefits. It allows clarity in assessing functional ability to undertake ADLs and IADLS. Monitoring and measuring the recovery achieved.',
            'Aim',
            'The aim of this course is to increase the understanding of how reablement impacts care practice, as operational staff begin to provide packages.',
            'Objectives',
            'Those who attend the course will be able to understand:'
        ],
        'description-ol': [
            'How reablement works',
            'The effects of person centred reablement on regaining independence',
            'How to support individuals to learn or relearn, to improve and to recover their abilities',
            'Identify the journey from referral and through the process of reablement',
            'Roles and responsibilities of all service staff',
            'Awareness of the potential for evidence-based risk taking and how it should be managed'
        ]
    },

    {
        title: 'Manual Handling of objects',
        price: '',
        'description-p': [
            "Moving and Handling is unique to each job, place and person. So is this course. If you want to improve your teams performance and want something something that's in plain English call. If you want another PowerPoint day click on the guys above or below on listing page.",
            'This course offers the practical information and skills needed to handle objects safely at work.',

            'By the end of the day those who attend will be able to:'
        ],
        'description-ol': [
            'Identify relevant legislation',
            'Conduct informal and formal risk assessments',
            'Identify and maintain a safer lifting posture',
            'The limits of individual capability',
            'Problem solving & practical handling of inanimate loads',
            'Team handling of inanimate objects'
        ]
    },
    {
        title: 'Train the trainer prevention and control of infection',
        price: '',
        'description-p': [],
        'description-ol': [
            'Identified principles of adult education in the workplace',
            'Plan and create courses for adults around the area of the prevention and control of infection in social care settings.',
            'Delivering effective training for the control and prevention of infection in social care settings'
        ]
    },
    {
        title: 'Dignity in Care',
        price: '',
        'description-p': [
            'By the end of the course those who attend will be able to:'
        ],
        'description-ol': [
            'Identify the importance of having zero tolerance of all forms of abuse\n',
            'Whistleblowing',
            'Organisational policy in whistleblowing'
        ]
    },
    {
        title: 'Memory Training',
        price: '',
        'description-p': [
            'What are Memory Training Sessions',
            'The aim of the course is teach those who attend the skills needed to assess, prepare, deliver an evaluate memory training sessions for older people.',
            'By the end of the course students will be able to:'
        ],
        'description-ol': [
            'Assess the needs of their service users with service appropriate tools',
            'Prepare and plan one to one and group memory training sessions',
            'Facilitate and deliver memory training sessions',
            'Identify partial and full exclusions to inclusion in memory training sessions',
            'Be able to critically reflect on their own practice'
        ]
    },

    {
        title: 'Parkinson’s Disease: Advanced Practice',
        price: '',
        'description-p': [
            'The course really is something very special, bespoke and unique. It offers a very in-depth package of study on and around the support and management of Parkinson’s in the community. As an organisation we are proud to deliver this package.',
            'The aim of the course is to provide those who attend an opportunity for an in-depth exploration of Parkinson’s Disease and how to effectively support this in community care settings.',
            'By the end of your studies you’ll be able to identify:'
        ],
        'description-ol': [
            'The realities from a personal and professional point of view',
            'The effects of the most common medications used for the management of Parkinson’s disease in the UK.',
            'Alternative therapies and the management of Parkinson’s disease',
            'Exercise management and prescription of Parkinson’s disease',
            'How all these elements come together in the effective support and management of 6. Parkinson’s in the community'
        ]
    },
    {
        title: 'Managing difficult staff',
        price: '',
        'description-p': [
            'The aim of this course is to give you the skills to manage underperforming or difficult staff to the best outcome for all. Subject areas include:'
        ],
        'description-ol': [
            'High, medium and low performance',
            'Hiding places of low performers',
            'Best way to obtain high performance'
        ]
    }
];

export default homeCareData;
